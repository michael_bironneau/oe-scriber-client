# OE Scriber Client Library #

The OE Scriber client library is intended to facilitate the development of real-time dashboards using Javascript. The name 'Scriber' is short for 'subscriber' - a reference to the publish/subscribe pattern employed here.

**Maintainer:** Michael Bironneau (michael.bironneau@openenergi.com)

###Feeds###
The feeds available at the moment are:

- Grid frequency
- Total available Kw (High + Low)
- Total response Kw (High + Low)
- Number of online sites
- Number of controllable loads
- Hotspots (TopoJSON data of available/responding sites' coordinates and Kw)
- UK Map (TopoJSON - requires scriber-ukdata.js)   

You can obtain an up-to-date list by running any of the examples and inspecting 'discovered feed' messages on the console output. Most feeds will be pushed to your client every few seconds. Hotspots are updated every 10 seconds. However: 

**Warning:** Scriber servers currently run off ArchiveDb, which is up to a minute behind live data and only updates frequency data every 10 seconds. In the near future they will run off ReportingDb and this problem will go away.

### How do I get set up? ###

Just grab the latest version of Scriber.min.js (see 'Source'). You can also clone the repository and try to run the examples. Scriber will need to connect to a server so if there isn't one running you may have to clone the 'Scriber Server' repository and follow the instructions there for getting started, first (or just as Michael to start one up).

### Contribution guidelines ###

This is a work in progress and changes (even breaking ones) are likely. The preferred method of contributing is to fork the repository, make your changes, and submit a pull request. If you don't want to do that permissions are set up so that you can push straight to the 'master' branch so you can do this if you wish, but if planning to use this workflow for any non-trivial changes please give Michael a heads up before pushing.

### Documentation ###

Head over to the 'wiki' page.

### Future work ###

Here are list of things that will (probably) get done in the near future, in reverse order of priority.

1. Support more feeds (high vs low, total power...)
2. Fallback to long polling for browsers without WebSocket support