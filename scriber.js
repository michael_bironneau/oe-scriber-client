/*
Open Energi Scriber Javascript client library
=============================================
Subscribes to a 'Scriber' topic of live data, including:
  -Grid frequency (Hz, float)
  -Online Sites (int)
  -Controllable Loads (int)
  -Availability (High + Low in MW, float)
  -Response (High + Low in MW, float)
  -Geographic availability map (topoJSON containing objects 'available' and 'provided' with 'value' property corresponding to availability/response in Kw)

On connection to a Scriber publisher a list of available topics are obtained.

::Author:: Michael Bironneau <michael.bironneau@openenergi.com>
::Version:: 0.2.04

.. note: Only websockets are supported for now. In the future there may be a long polling fallback if this is required.

Version History
---------------
0.2.04: Bug fix (unsubscribe requests with wildcards getting ignored)
0.2.03: Bug fix (scope of iterators), supports subscribing to multiple, comma-separated list of topics
0.2.02: Supports wildcard filtering on server side
0.2.01: Fixed bug where '*' subscriptions are ignored
0.2.00: Added topic discovery, connection to multiple Scriber publishers via a Scriber broker and server-side subscription filtering
0.1.01: Added onClose handler

*/

//Namespace
if (OEScriber === undefined) {
	var OEScriber = {};
}

OEScriber.brokerURL = 'http://rlteclab1:7002/'; //Scriber broker endpoint
OEScriber.subscriptions = {}; //List of {topic: callback}
OEScriber.publishers = []; //List of WebSocket objects
OEScriber.connections = []; //List of open connections
OEScriber.topics = []; //List of available topics. This is not kept up to date if connections get closed. TODO: Keep it up to date.

//Callbacks
OEScriber.errCallback = null;
OEScriber.closedCallback = null;
OEScriber.newTopicCallback = null;

OEScriber.onConnectionError = function(callback) {
	OEScriber.errCallback = callback;
}; //Callback for non-fatal connection errors

OEScriber.onClose = function(callback) {
	OEScriber.closedCallback = callback;
}; //Callback for when the websocket is closed

OEScriber.onNewTopic = function(callback) {
	OEScriber.newTopicCallback = callback;
}; //Callback when new topic is discovered

//Register callback for websocket message reception on topic(s)
//topics can be comma-separated list
OEScriber.subscribe = function(topic, callback, err_callback) {
	if (topic.indexOf(',') == -1) {
		OEScriber.subscribe_one(topic, callback, err_callback);
	}
	else {
		var topic_list = topic.split(',');
		for (var i=0; i<topic_list.length; i++) {
			OEScriber.subscribe_one(topic_list[i].trim(), callback, err_callback);
		}
	}
};

//Unsubscribe from feed
//topic can be comma-separated list
OEScriber.unsubscribe = function(topic) {
	if (topic.indexOf(',') == -1) {
		OEScriber.unsubscribe_one(topic);
	}
	else {
		var topic_list = topic.split(',');
		for (var i=0; i<topic_list.length; i++) {
			OEScriber.unsubscribe_one(topic_list[i].trim());
		}
	}
};


OEScriber.subscribe_one = function(topic, callback, err_callback) {
	var parent_topic = topic.indexOf('.') == -1? topic: topic.substring(0, topic.indexOf('.'));
	if (parent_topic != '*' && typeof OEScriber.topics.indexOf(parent_topic) == -1) throw new Error('Unknown parent topic ' + parent_topic + '!');
	OEScriber.subscriptions[topic] = callback;
	var success = false;
	for (var i=0; i<OEScriber.connections.length; i++) {
		if (OEScriber.connections[i].topics.indexOf(parent_topic) !== -1) {
			try {
				OEScriber.connections[i].ws.send('SUBSCRIBE ' + topic);
				success = true; //We subscribed to one feed - notify user?
			}
			catch (err) {
				if (typeof OEScriber.errCallback !== null) OEScriber.errCallback(err); //Non-fatal
			}
		}
	}
	if (!success && typeof(err_callback) !== 'undefined') err_callback(new Error('Could not subscribe to any feed, or no feeds are available.'));
};

OEScriber.unsubscribe_one = function(topic) {
	var parent_topic = topic.indexOf('.') == -1? topic: topic.substring(0, topic.indexOf('.'));
	if (typeof OEScriber.subscriptions[topic] == 'undefined') return; //We weren't subscribed anyway
	OEScriber.subscriptions[topic] = null;
	for (var i=0; i<OEScriber.connections.length; i++) {
		if (OEScriber.connections[i].topics.indexOf(parent_topic) !== -1) {
			try {
				OEScriber.connections[i].ws.send('UNSUBSCRIBE ' + topic);
			}
			catch (err) {
				if (typeof OEScriber.errCallback !== null) OEScriber.errCallback(err); //Non-fatal
			}
		}
	}
};

//Internal helper method - execute callback
OEScriber.callBack = function(subscription, topic, data) {
	if (OEScriber.subscriptions[subscription] === null || typeof(OEScriber.subscriptions[subscription])== 'undefined') return;
	OEScriber.subscriptions[subscription](data, topic); //Change order of parameters around so client code can ignore topic if it wants
};

//Handle incoming messages
OEScriber.handleMessage = function(url, msg) {
	if (typeof(msg.topics) !== 'undefined') {
		//Handle frequency message
		conn_id = OEScriber.is_connected(url);
		if (conn_id == -1 && typeof(OEScriber.errCallback) !== null) OEScriber.errCallback(new Error('Unexpected: received message from connection not in pool.'));
		if (conn_id != -1) OEScriber.connections[conn_id].topics = msg.topics; //Update topics
		for (var i=0; i<msg.topics.length; i++) {
			if (OEScriber.topics.indexOf(msg.topics[i]) == -1) {
				OEScriber.topics.push(msg.topics[i]); //Add to our topic list
				if (OEScriber.newTopicCallback !== null) OEScriber.newTopicCallback(msg.topics[i]); //Notify client code of discovery
			}
		}
	}
	else if (typeof(msg.topic) !== 'undefined' && typeof(msg.message) != 'undefined' && typeof(msg.subscription) != 'undefined') {
		//Handle feed topic
		OEScriber.callBack(msg.subscription, msg.topic, msg.message);
	}
	else {
		OEScriber.errCallback(url, new Error('Unknown message format'));
	}
};

//Connect and bind event handlers
OEScriber.connect_to_publisher = function(url, callback) {
	conn_id = OEScriber.is_connected(url); //Check if we are already connected
	if (conn_id !== -1) {
		//Drop connection first and remove it from the pool
		OEScriber.connections[conn_id].ws.close();
		OEScriber.splice(conn_id,1);

	}

	ws = new WebSocket(url);
	
	ws.onopen = function (event) {
		OEScriber.connections.push({url: url, ws: ws, topics: []}); //Add to connection pool
		if (typeof(callback) !== 'undefined') callback(url);
	};
	
	ws.onerror = function(error) {
		if (OEScriber.errCallback !== null) OEScriber.errCallback(url, error);
	};

	ws.onclose = function() {
		OEScriber.connected = false;
		conn_ix = OEScriber.is_connected(url);
		if (conn_ix != -1) OEScriber.connections.splice(conn_ix, 1); //remove from connection pool
		if (OEScriber.closedCallback !== null) OEScriber.closedCallback(url); //notify user code
		if (OEScriber.connections.length===0 && OEScriber.errCallback !== null) OEScriber.errCallback(null, new Error('No publishers available'));
	};

	ws.onmessage = function(event) {
		data = JSON.parse(event.data);
		try{
			OEScriber.handleMessage(url, data);
		}
		catch (err) {
			if (OEScriber.errCallback !== null) {
				OEScriber.errCallback(url, err);
				}
			else {
				throw err;
			}
		}
		
	};
};

//Connect to a list of publishers without contacting a broker to find out if they are available first
OEScriber.connectBrokerless = function(publishers, callbacks) {
	OEScriber.publishers = publishers;
	if (OEScriber.publishers.length === 0) {
		var err = new Error('No Scriber publishers are currently available.');

		if (typeof(callbacks) !== 'undefined' && typeof(callbacks.error) !== 'undefined') {
			callbacks.error(err);
		}
		else {
			throw err;
		}
	}
	var publisher_url;
	for (var i=0; i<OEScriber.publishers.length; i++) {
		if (OEScriber.publishers[i].address.indexOf('::1') === 0) {
			//Hack for IPv6 presentation of localhost
			publisher_url ='ws://localhost:' + OEScriber.publishers[i].port.toString();
		} else {
			//Anything else
			publisher_url = 'ws://' + OEScriber.publishers[i].address + ':' + OEScriber.publishers[i].port.toString();
		}
		if (typeof(callbacks) !== 'undefined') {
			OEScriber.connect_to_publisher(publisher_url, callbacks.partial);
		} else {
			OEScriber.connect_to_publisher(publisher_url);
		}
	}


};

//Synchronously connect to broker and get list of Scriber publishers
//Success callback is executed when connection to all publishers has been made
//Error callback is executed if a fatal error occurs (can't connect to broker)
//Partial callback is executed on success of connection with each individual publisher
OEScriber.connect = function(callbacks) {
	var xmlHttp = null;
	xmlHttp = new XMLHttpRequest();
	xmlHttp.open( "GET", OEScriber.brokerURL, false );
	try {
		xmlHttp.send( null );
		OEScriber.connectBrokerless(JSON.parse(xmlHttp.responseText), callbacks);
	} catch (err) {
		if (typeof(callbacks) !== 'undefined' && typeof(callbacks.error) !== 'undefined') {
			callbacks.error(err);
		}
		else {
			throw new Error(err);
		}
	}
	if (typeof(callbacks) !== 'undefined' && typeof(callbacks.success) !== 'undefined') {
		callbacks.success();
	}

};

//Close connection to optional 'url'. If 'url' is not specified, close all connections.
OEScriber.disconnect = function(url) {
	if (typeof(url) == 'undefined') {
		//Disconnect all connections
		for (var i=0; i<OEScriber.connections.length; i++) {
			OEScriber.connections[i].ws.close();
		}
		OEScriber.connections = []; //Clear connection pool
	}
	else {
		conn_id = OEScriber.is_connected(url);
		if (conn_id !== -1) {
			//Close and remove from pool
			OEScriber.connections[conn_id].close();
			OEScriber.connections.splice(conn_id,1);
		}
	}
};

//Returns -1 (not connected) or index of connection in OEScriber.connections
OEScriber.is_connected = function(url) {
	for (var i=0; i<OEScriber.connections.length; i++) {
		if (OEScriber.connections[i].url == url) return i;
	}
	return -1;
};